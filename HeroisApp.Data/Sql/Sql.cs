﻿using HeroisApp.Core.Interfaces.Sql;

namespace HeroisApp.Data.Sql
{
    public class Sql : ISql
    {
        private readonly string _connectionString;

        public Sql(string connectionString) => _connectionString = connectionString;

        public string ObterConnectionString() => _connectionString;

    }
}
