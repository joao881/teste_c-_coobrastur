﻿using Dapper;
using HeroisApp.Core.Entidades;
using HeroisApp.Core.Interfaces.Repository;
using HeroisApp.Core.Interfaces.Sql;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace HeroisApp.Data.Repository
{
    public class HeroiRepository : IHeroiRepository
    {
        private readonly ISql _sql;
        public HeroiRepository(ISql sql)
        {
            _sql = sql;
        }

        public async Task<List<int>> RetornaIdsHerois()
        {
            var sqlQuery = "SELECT ID FROM [tblHerois]";

            using var connection = new SqlConnection(_sql.ObterConnectionString());
            var ids = await connection.QueryAsync<int>(sqlQuery);

            return ids.AsList();
        }
        public async Task<Heroi> RetornaHeroiPorId(int id)
        {
            var param = new
            {
                in_id = id,
            };

            using var connection = new SqlConnection(_sql.ObterConnectionString());
            var heroi = await connection.QueryFirstOrDefaultAsync<Heroi>("sp_retorna_heroi", param, commandType: CommandType.StoredProcedure);

            return heroi;
        }

        public async Task<Heroi> RetornaHeroiPorIdentidade(string in_nome)
        {
            var param = new
            {
                in_nome = in_nome,
            };

            using var connection = new SqlConnection(_sql.ObterConnectionString());
            var heroi = await connection.QueryFirstOrDefaultAsync<Heroi>("sp_retorna_heroi", param, commandType: CommandType.StoredProcedure);

            return heroi;
        }

        public async Task<Heroi> RetornaHeroiPorNome(string nome)
        {
            var sqlQuery = "SELECT * FROM [tblHerois] WHERE NOME = @nome";

            using var connection = new SqlConnection(_sql.ObterConnectionString());
            var heroi = await connection.QueryFirstOrDefaultAsync<Heroi>(sqlQuery, new { nome });

            return heroi;
        }

        public async Task<bool> CadastraAtualizaHeroi(Heroi heroi)
        {
            var param = new
            {
                in_id = heroi.Id,
                in_nome = heroi.Nome,
                in_identidadeSecreta = heroi.IdentidadeSecreta,
                in_inteligencia = heroi.Inteligencia,
                in_forca = heroi.Forca,
                in_velocidade = heroi.Velocidade,
                in_durabilidade = heroi.Durabilidade,
                in_poder = heroi.Poder,
                in_combate = heroi.Combate
            };

            using var connection = new SqlConnection(_sql.ObterConnectionString());
            await connection.ExecuteAsync("sp_cadastra_heroi", param, commandType: CommandType.StoredProcedure);

            return true;
        }
    }
}
