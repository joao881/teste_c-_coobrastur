﻿using HeroisApp.Core.Entidades;
using HeroisApp.Core.InputModels;

namespace HeroisApp.Core.Util
{
    public static class HeroiMap
    {
        public static Heroi MapeiaHeroiInput(HeroiInputModel heroiInputModel)
        {
            int id;
            if (heroiInputModel.Id == null)
                id = 0;

            else
                id = (int)heroiInputModel.Id;


            return new Heroi
            {
                Combate = heroiInputModel.Combate,
                Durabilidade = heroiInputModel.Durabilidade,
                Forca = heroiInputModel.Forca,
                Id = id,
                IdentidadeSecreta = heroiInputModel.IdentidadeSecreta,
                Inteligencia = heroiInputModel.Inteligencia,
                Nome = heroiInputModel.Nome,
                Poder = heroiInputModel.Poder,
                Velocidade = heroiInputModel.Velocidade
            };
        }
    }
}
