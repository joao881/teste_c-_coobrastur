﻿namespace HeroisApp.Core.Util
{
    public class RespostaApi
    {
        public bool Sucesso { get; set; }
        public string Mensagem { get; set; }
        public object ObjetoRetorno { get; set; }
    }
}
