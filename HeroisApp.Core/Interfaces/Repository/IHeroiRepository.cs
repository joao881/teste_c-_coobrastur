﻿using HeroisApp.Core.Entidades;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HeroisApp.Core.Interfaces.Repository
{
    public interface IHeroiRepository
    {
        public Task<List<int>> RetornaIdsHerois();
        public Task<Heroi> RetornaHeroiPorId(int id);
        public Task<Heroi> RetornaHeroiPorIdentidade(string identidade);
        public Task<Heroi> RetornaHeroiPorNome(string nome);
        public Task<bool> CadastraAtualizaHeroi(Heroi heroi);
    }
}

