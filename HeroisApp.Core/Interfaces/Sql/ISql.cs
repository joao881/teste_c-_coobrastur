﻿namespace HeroisApp.Core.Interfaces.Sql
{
    public interface ISql
    {
        string ObterConnectionString();
    }
}
