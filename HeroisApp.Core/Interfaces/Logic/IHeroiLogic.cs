﻿using HeroisApp.Core.InputModels;
using HeroisApp.Core.Util;
using System.Threading.Tasks;

namespace HeroisApp.Core.Interfaces.Logic
{
    public interface IHeroiLogic
    {
        public Task<RespostaApi> RetornaHerois();
        public Task<RespostaApi> RetornaHeroiPorNomeOuIdentidade(string identidade, string nome);
        public Task<RespostaApi> CriarOuEditarHeroi(HeroiInputModel heroiInputModel);
        public Task<RespostaApi> RetornaHeroisOrdenados();

    }
}
