﻿namespace HeroisApp.Core.Entidades
{
    public class Heroi
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string IdentidadeSecreta { get; set; }
        public int Inteligencia { get; set; }
        public int Forca { get; set; }
        public int Velocidade { get; set; }
        public int Durabilidade { get; set; }
        public int Poder { get; set; }
        public int Combate { get; set; }
    }
}
