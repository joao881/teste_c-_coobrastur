﻿using HeroisApp.Core.InputModels;
using HeroisApp.Core.Interfaces.Logic;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace HeroisApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HeroisController : ControllerBase
    {
        private readonly IHeroiLogic _heroiLogic;

        public HeroisController(IHeroiLogic heroiLogic)
        {
            _heroiLogic = heroiLogic;
        }

        [HttpGet("BuscarHerois")]
        public async Task<IActionResult> ObtemHerois()
        {
            var resposta = await _heroiLogic.RetornaHerois();

            if (resposta.Sucesso)
                return Ok(resposta);
            else
                return BadRequest(resposta);
        }

        [HttpGet("BuscaPorIdentidadeOuNome")]
        public async Task<IActionResult> ObtemHeroiPorNomeOuIdentidadeSecreta([FromQuery] string nome, [FromQuery] string identidadeSecreta)
        {
            var resposta = await _heroiLogic.RetornaHeroiPorNomeOuIdentidade(identidadeSecreta, nome);

            if (resposta.Sucesso)
                return Ok(resposta);
            else
                return BadRequest(resposta);
        }

        [HttpPost("CriaEditaHeroi")]
        public async Task<IActionResult> CriaOuEditaHeroi([FromBody] HeroiInputModel heroiInputModel)
        {
            var resposta = await _heroiLogic.CriarOuEditarHeroi(heroiInputModel);

            if (resposta.Sucesso)
                return Ok(resposta);
            else
                return BadRequest(resposta);

        }

        [HttpGet("BuscaHeroiOrdenados")]
        public async Task<IActionResult> ObtemHeroisOrdernados()
        {
            var resposta = await _heroiLogic.RetornaHeroisOrdenados();

            if (resposta.Sucesso)
                return Ok(resposta);
            else
                return BadRequest(resposta);
        }
    }
}
