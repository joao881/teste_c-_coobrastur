﻿using HeroisApp.Core.Entidades;
using HeroisApp.Core.InputModels;
using HeroisApp.Core.Interfaces.Logic;
using HeroisApp.Core.Interfaces.Repository;
using HeroisApp.Core.Util;
using HeroisApp.Logic.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HeroisApp.Logic.Logic
{
    public class HeroiLogic : IHeroiLogic
    {
        private readonly IHeroiRepository _heroiRepository;
        public HeroiLogic(IHeroiRepository heroiRepository)
        {
            _heroiRepository = heroiRepository;
        }

        public async Task<RespostaApi> RetornaHerois()
        {
            try
            {
                var listaHerois = new List<Heroi>();
                var idsHerois = await _heroiRepository.RetornaIdsHerois();

                foreach (var id in idsHerois)
                {
                    var heroi = await _heroiRepository.RetornaHeroiPorId(id);
                    listaHerois.Add(heroi);
                }

                var resposta = new RespostaApi
                {
                    Sucesso = true,
                    Mensagem = "Busca realizada com sucesso",
                    ObjetoRetorno = listaHerois.OrderBy(x => x.Nome)
                };

                return resposta;

            }
            catch (Exception ex)
            {

                return new RespostaApi
                {
                    Sucesso = false,
                    Mensagem = $"Erro; {ex.Message}",
                    ObjetoRetorno = null
                };
            }
        }

        public async Task<RespostaApi> RetornaHeroiPorNomeOuIdentidade(string identidade, string nome)
        {
            try
            {
                var heroi = !string.IsNullOrEmpty(identidade) ?
                   await _heroiRepository.RetornaHeroiPorIdentidade(identidade) :
                   await _heroiRepository.RetornaHeroiPorNome(nome);

                var resposta = new RespostaApi
                {
                    Sucesso = true,
                    Mensagem = "Busca realizada com sucesso",
                    ObjetoRetorno = heroi
                };

                return resposta;
            }
            catch (Exception ex)
            {

                return new RespostaApi
                {
                    Sucesso = false,
                    Mensagem = $"Erro; {ex.Message}",
                    ObjetoRetorno = null
                };
            }

        }

        public async Task<RespostaApi> CriarOuEditarHeroi(HeroiInputModel heroiInputModel)

        {
            try
            {
                var novoHeroi = HeroiMap.MapeiaHeroiInput(heroiInputModel);
                novoHeroi.Id = await HeroiExisteBase(novoHeroi.Id) ? novoHeroi.Id : await ObtemNovoIdHeroi();

                var retornoAtualizacaoCadastro = await _heroiRepository.CadastraAtualizaHeroi(novoHeroi);

                var resposta = new RespostaApi
                {
                    Sucesso = true,
                    Mensagem = "Cadastro ou Atualização realizada com sucesso",
                    ObjetoRetorno = null
                };

                return resposta;
            }
            catch (Exception ex)
            {

                return new RespostaApi
                {
                    Sucesso = false,
                    Mensagem = $"Erro; {ex.Message}",
                    ObjetoRetorno = null
                };
            }
        }

        public async Task<RespostaApi> RetornaHeroisOrdenados()
        {
            try
            {
                var listaHerois = new List<Heroi>();

                for (int i = 1; i < 31; i++)
                {
                    var heroiApi = await Rest.ObtemHeroi(i);

                    var novoHeroi = new Heroi
                    {
                        Nome = heroiApi.name,
                        Id = heroiApi.id != "null" ? Convert.ToInt32(heroiApi.id) : 0,
                        IdentidadeSecreta = heroiApi.biography.aliases[0],
                        Forca = heroiApi.powerstats.strength != "null" ? Convert.ToInt32(heroiApi.powerstats.strength) : 0,
                        Velocidade = heroiApi.powerstats.speed != "null" ? Convert.ToInt32(heroiApi.powerstats.speed) : 0,
                        Inteligencia = heroiApi.powerstats.intelligence != "null" ? Convert.ToInt32(heroiApi.powerstats.intelligence) : 0,
                        Combate = heroiApi.powerstats.combat != "null" ? Convert.ToInt32(heroiApi.powerstats.combat) : 0,
                        Durabilidade = heroiApi.powerstats.durability != "null" ? Convert.ToInt32(heroiApi.powerstats.durability) : 0,
                        Poder = heroiApi.powerstats.power != "null" ? Convert.ToInt32(heroiApi.powerstats.power) : 0,
                    };

                    listaHerois.Add(novoHeroi);
                }

                var resposta = new RespostaApi
                {
                    Sucesso = true,
                    Mensagem = "Busca realizada com sucesso",
                    ObjetoRetorno = listaHerois.OrderBy(x => x.IdentidadeSecreta)
                };

                return resposta;
            }
            catch (Exception ex)
            {

                return new RespostaApi
                {
                    Sucesso = false,
                    Mensagem = $"Erro; {ex.Message}",
                    ObjetoRetorno = null
                };
            }
        }
        private async Task<int> ObtemNovoIdHeroi()
        {
            var idsHerois = await _heroiRepository.RetornaIdsHerois();

            if (idsHerois.Count == 0)
                return 1;
            else
                return idsHerois.OrderByDescending(x => x).First() + 1;

        }
        private async Task<bool> HeroiExisteBase(int idHeroi)
        {
            var heroi = await _heroiRepository.RetornaHeroiPorId(idHeroi);
            var heroiExiste = heroi != null;

            return heroiExiste;
        }
    }
}
