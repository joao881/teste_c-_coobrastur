﻿using HeroisApp.Core.Modelos;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace HeroisApp.Logic.Service
{
    public static class Rest
    {
        private static readonly string _url = "https://www.superheroapi.com/api.php/";
        private static readonly string _token = "";

        public static async Task<ApiHeroi> ObtemHeroi(int id)
        {
            var client = new HttpClient
            {
                BaseAddress = new System.Uri($"{_url}")
            };

            var resposta = await client.GetAsync($"{_token}/{id}");
            var conteudo = await resposta.Content.ReadAsStringAsync();

            return JsonSerializer.Deserialize<ApiHeroi>(conteudo);
        }
    }
}
